package fr.istic.pit.frontend.ui.fragments;

import android.Manifest;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.config.Configuration;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.FolderOverlay;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.Overlay;
import org.osmdroid.views.overlay.Polyline;
import org.osmdroid.views.overlay.compass.CompassOverlay;
import org.osmdroid.views.overlay.compass.InternalCompassOrientationProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.istic.pit.frontend.POIUtils;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.Drone;
import fr.istic.pit.frontend.model.DroneMission;
import fr.istic.pit.frontend.model.DronePoint;
import fr.istic.pit.frontend.model.Mapper;
import fr.istic.pit.frontend.model.PointOfInterest;
import fr.istic.pit.frontend.model.Resource;
import fr.istic.pit.frontend.service.DroneService;
import fr.istic.pit.frontend.service.PointOfInterestService;
import fr.istic.pit.frontend.service.RessourceService;
import fr.istic.pit.frontend.service.sync.SynchroClientService;
import fr.istic.pit.frontend.service.sync.UpdateEventType;
import fr.istic.pit.frontend.ui.markers.DronePointMarker;
import fr.istic.pit.frontend.ui.markers.MyRadiusMarkerClusterer;
import fr.istic.pit.frontend.ui.markers.POIMarkerWithLabel;

/**
 * Fragment pour la vue du drone
 */
public class DroneFragment extends Fragment implements DroneParcoursFragment.DroneParcoursEventListener, DronePointMarker.DronePointMarkerListener {

    @BindView(R.id.carteDrone)
    MapView mapDrone;
    @BindView(R.id.droneButtonDisplayParcours)
    Button displayParcours;
    @BindView(R.id.droneParcoursPlaceholder)
    FrameLayout parcoursPlaceHolder;
    @BindView(R.id.droneButtonDisplayVideo)
    Button displayVideo;
    @BindView(R.id.droneVideoPlaceholder)
    FrameLayout videoPlaceHolder;
    @BindView(R.id.drone_map_center)
    View mapCenter;

    private Context context;

    private SitacViewFragment sitac;
    private DroneParcoursFragment droneParcoursFragment;
    private DroneVideoFragment droneVideoFragment;

    private DroneMission mission;
    private FolderOverlay folderLocations;
    private MyRadiusMarkerClusterer folderResources;
    private MyRadiusMarkerClusterer folderPOI;
    private Polyline route;
    private Marker droneMarker;
    private Long interventionId;
    private boolean isEditable;
    private boolean isEditing;

    private DroneService droneService;
    private SynchroClientService synchroClientService;
    private PointOfInterestService pointOfInterestService;
    private RessourceService resourceService;


    public DroneFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // No refresh before creation
        setUserVisibleHint(false);

        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_drone, container, false);
        ButterKnife.bind(this,v);

        interventionId = getArguments().getLong("interventionId");
        isEditable = getArguments().getBoolean("droneAvailable");
        isEditing = false;
        setMapCenter();

        mission = new DroneMission();
        route = new Polyline();

        droneParcoursFragment = new DroneParcoursFragment(this, interventionId);
        droneVideoFragment = new DroneVideoFragment(interventionId);
        getChildFragmentManager().beginTransaction().add(R.id.droneParcoursPlaceholder, droneParcoursFragment, "ParcoursTag").commit();
        getChildFragmentManager().beginTransaction().add(R.id.droneVideoPlaceholder, droneVideoFragment, "VideoTag").commit();

        //chargement de la carte
        loadMap();

        // Creation des services
        droneService = DroneService.getService(getContext());
        synchroClientService = SynchroClientService.getService(getContext());
        pointOfInterestService = PointOfInterestService.getService(getContext());
        resourceService = RessourceService.getService(getContext());

        getMission();
        getResourcesMap();
        getPOIsMap();
        initSynchro();

        return v;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && sitac!=null && sitac.getMap()!=null) {
            MapView mapSitac = sitac.getMap();
            this.mapDrone.getController().setCenter(mapSitac.getMapCenter());
            this.mapDrone.getController().setZoom(mapSitac.getZoomLevelDouble());
        }
        if(isVisibleToUser && null!=droneVideoFragment){
            droneVideoFragment.setShouldDisplay(videoPlaceHolder.getVisibility() == View.VISIBLE);
        }
    }

    @Override
    public void onNewOrder() {
        onMapPointUpdate();
        mapDrone.invalidate();
    }

    @Override
    public void onCloseFragment() {
        hideParcoursFragment();
    }

    @Override
    public List<DronePoint> getPoints() {
        return mission.points;
    }

    @Override
    public void onMissionStartPause() {
        isEditing = !isEditing;
        setMapCenter();
        setDronePointEditionMode(isEditing());
        if(!isEditing()) { // Nous étions en édition précedemment
            try {
                droneService.createMission(
                        interventionId,
                        mission.missionLoop,
                        getPoints(),
                        response -> {},
                        error -> {
                            if(error.networkResponse.statusCode == 409) {
                                isEditing = !isEditing;
                                setDronePointEditionMode(isEditing());
                                droneParcoursFragment.updateDisplayedButtons(true);
                                Toast.makeText(getContext(), "Le drone n'est pas encore arrivé à son lieu de lancement.", Toast.LENGTH_LONG).show();
                            }
                        });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            droneService.stopMission(
                    interventionId,
                    response -> {},
                    Throwable::printStackTrace);
        }
    }

    @Override
    public void onMissionStop() {
        isEditing = true;
        setMapCenter();
        setDronePointEditionMode(isEditing());
        droneService.rtlMission(
                interventionId,
                response -> {},
                Throwable::printStackTrace);
    }

    @Override
    public void onDeletePoint(DronePoint dronePoint) {
        for(Overlay marker : folderLocations.getItems()){
            DronePointMarker m = (DronePointMarker)marker;
            if(m.getDronePoint().equals(dronePoint)){
                folderLocations.getItems().remove(marker);
                break;
            }
        }
        onMapPointUpdate();
        mapDrone.invalidate();
    }

    @Override
    public void setMissionLoop(boolean missionLoop) {
        mission.missionLoop = missionLoop;
        onMapPointUpdate();
    }

    public void clearDronePoint() {
        List<Overlay> items = folderLocations.getItems()
                .stream()
                .filter(overlay -> !(overlay instanceof DronePointMarker))
                .collect(Collectors.toList());
        mission.points.clear();
        folderLocations.getItems().clear();
        folderLocations.getItems().addAll(items);
        mapDrone.invalidate();
    }

    @Override
    public void onMissionAddPoint() {
        DronePoint point = new DronePoint();
        point.latitude = mapDrone.getMapCenter().getLatitude();
        point.longitude = mapDrone.getMapCenter().getLongitude();
        point.missionId = mission.id;
        point.orderNumber = getPoints().size();
        point.id = getPoints().size();
        getPoints().add(point);
        DronePointMarker marker = new DronePointMarker(context,this,point);
        folderLocations.add(marker);
        onMapPointUpdate();
    }

    @Override
    public boolean isEditable() {
        return isEditable;
    }

    @Override
    public boolean isEditing() {
        return isEditing;
    }

    @Override
    public Drone.State getDroneState() {
        return mission.drone != null ? mission.drone.state : Drone.State.GROUND;
    }

    @OnClick(R.id.droneButtonDisplayParcours)
    void displayParcoursFragment(){
        displayParcours.setVisibility(View.GONE);
        parcoursPlaceHolder.setVisibility(View.VISIBLE);
    }

    private void hideParcoursFragment(){
        displayParcours.setVisibility(View.VISIBLE);
        parcoursPlaceHolder.setVisibility(View.GONE);
    }

    @OnClick(R.id.droneButtonDisplayVideo)
    void displayHideVideoFragment(){
        if(videoPlaceHolder.getVisibility()==View.GONE){
            videoPlaceHolder.setVisibility(View.VISIBLE);
            displayVideo.setText("/\n\\");
            droneVideoFragment.setShouldDisplay(true);
        }else{
            videoPlaceHolder.setVisibility(View.GONE);
            displayVideo.setText("\\\n/");
            droneVideoFragment.setShouldDisplay(false);
        }
    }

    private void setDronePointEditionMode(boolean editing){
        for(Overlay marker : folderLocations.getItems()){
            ((DronePointMarker)marker).setDraggable(editing);
        }
    }

    /**
     * Chargement de la carte et initialisation des folders d'Overlays
     */
    private void loadMap() {
        //permission de localisation et de lecture de mémoire pour récup la carte
        ActivityCompat.requestPermissions(Objects.requireNonNull(getActivity()),
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        //chargement carte
        Configuration.getInstance().load(getActivity().getApplicationContext(), PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext()));
        //positionnement sur la carte (zoom + emplacement) et affichage orientation via boussole
        mapDrone.getController().setZoom(15.0);
        mapDrone.getController().setCenter(new GeoPoint(48.1136782,-1.6336046));
        mapDrone.setMultiTouchControls(true);
        mapDrone.setDestroyMode(false);

        CompassOverlay compassOverlay = new CompassOverlay(getActivity().getApplicationContext(), new InternalCompassOrientationProvider(getActivity().getApplicationContext()), mapDrone);
        compassOverlay.enableCompass();
        mapDrone.getOverlays().add(compassOverlay);

        folderLocations = new FolderOverlay();
        folderResources = new MyRadiusMarkerClusterer(context, context.getDrawable(R.drawable.seul_cluster));
        folderPOI = new MyRadiusMarkerClusterer(context, context.getDrawable(R.drawable.poi_cluster));
        mapDrone.getOverlays().add(folderResources);
        mapDrone.getOverlays().add(folderPOI);
        mapDrone.getOverlays().add(folderLocations);
    }

    private void setMapCenter(){
        if (isEditing){
            mapCenter.setVisibility(View.VISIBLE);
        } else {
            mapCenter.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onMapPointUpdate(){
        //draw lines
        mapDrone.getOverlays().remove(route);
        route.getOutlinePaint().setColor(Color.parseColor("#000000"));
        route.getOutlinePaint().setStrokeWidth(2);
        List<GeoPoint> routePoints = new ArrayList<>();
        for(DronePoint point : getPoints()){
            routePoints.add(new GeoPoint(point.latitude, point.longitude));
        }
        if (!routePoints.isEmpty() && mission.missionLoop)
            routePoints.add(routePoints.get(0));
        route.setPoints(routePoints);
        mapDrone.getOverlays().add(route);
        mapDrone.invalidate();
    }

    @Override
    public MapView getMap() {
        return this.mapDrone;
    }

    private void updateDroneIcon() {
        switch(mission.drone.state){
            case IDLE:droneMarker.setIcon(context.getDrawable(R.drawable.drone_aerostationnement));break;
            case GROUND:droneMarker.setIcon(context.getDrawable(R.drawable.drone_au_sol));break;
            case MOVING:droneMarker.setIcon(context.getDrawable(R.drawable.drone_deplacement));break;
            default:break;
        }
    }

    public void setSitac(@NonNull SitacViewFragment sitac){
        this.sitac = sitac;
        sitac.setDroneFragment(this);
    }

    MapView getMapDrone(){
        return this.mapDrone;
    }

    private void initSynchro() {
        synchroClientService.subscribeTo(interventionId, event -> {
            UpdateEventType type = event.getType();
            switch (type) {
                case MISSION_CREATE:
                    isEditing = false;
                    setMapCenter();
                    clearDronePoint();
                    droneParcoursFragment.clearPoints();
                    droneParcoursFragment.updateDisplayedButtons(true);
                    getMission();
                    break;
                case DRONE_STATE:
                    try {
                        JSONObject data = event.getData();
                        updateDronePosition(
                                data.getDouble("latitude"),
                                data.getDouble("longitude"),
                                data.getDouble("altitude"),
                                data.getString("state"));
                    } catch (JSONException e) {
                        e.printStackTrace(); // Should never happen
                    }
                    break;
                case EMERGENCY_RTL:
                    isEditing = false;
                    setMapCenter();
                    isEditable = false;
                    setDronePointEditionMode(isEditing());
                    droneParcoursFragment.updateDisplayedButtons();
                    Toast.makeText(context, "Batterie trop faible! Le drone a quitté l'intervention et rentre à son lieu de lancement.", Toast.LENGTH_LONG).show();
                    break;
                case POI_CREATE:
                case POI_UPDATE:
                case POI_DISABLE:
                    getPOIsMap();
                    break;
                case RESOURCE_UPDATE:
                case RESOURCE_MOVE:
                case RESOURCE_ARRIVE:
                case RESOURCE_RELEASE:
                case RESOURCE_REQUEST_ACCEPT:
                case RESOURCE_REQUEST_REJECT:
                case RESOURCE_REQUEST_CREATE:
                    getResourcesMap();
                    break;
                default:
                    break;
            }
        });
    }

    private void updateDronePosition(double latitude, double longitude, double altitude, String state) {
        if(mission.drone == null)
            mission.drone = new Drone();
        mission.drone.latitude = latitude;
        mission.drone.longitude = longitude;
        mission.drone.altitude = altitude;
        mission.drone.state = Drone.State.valueOf(state);

        if(droneMarker == null) {
            droneMarker = new Marker(mapDrone);
            droneMarker.setPosition(new GeoPoint(mission.drone.latitude, mission.drone.longitude));
            droneMarker.setAnchor((float) 0.5, (float) 0.5);
            mapDrone.getOverlays().add(droneMarker);
        } else {
            droneMarker.setPosition(new GeoPoint(mission.drone.latitude, mission.drone.longitude));
        }
        updateDroneIcon();
        droneParcoursFragment.updateDisplayedButtons();

        mapDrone.invalidate();
    }

    private void getMission() {
        droneService.getMission(interventionId,
                response -> {
                    try {
                        mission = Mapper.parseDroneMission(response);
                    } catch (JSONException e) {
                        // mission is null > empty mission
                    }
                    for(DronePoint point : mission.points){
                        DronePointMarker m = new DronePointMarker(context, this, point);
                        folderLocations.add(m);
                    }
                    isEditing = isEditable() && mission.terminated;
                    setMapCenter();
                    droneParcoursFragment.updatePoints();
                    droneParcoursFragment.updateDisplayedButtons(true);
                    setDronePointEditionMode(isEditing());
                    onMapPointUpdate();
                },
                Throwable::printStackTrace);
    }

    private void getPOIsMap() {
        pointOfInterestService.getPOIList(
                interventionId,
                response -> {
                    folderPOI.getItems().clear();
                    try {
                        List<PointOfInterest> pointOfInterests = Mapper.parsePointOfInterest(response);
                        for(PointOfInterest pointOfInterest : pointOfInterests)
                            try {
                                folderPOI.getItems().add(createMarkerPOI(pointOfInterest));
                            }  catch (NullPointerException  e) {
                                e.printStackTrace();
                                Toast.makeText(context,"Une erreur de création de marker s'est produite",Toast.LENGTH_SHORT).show();
                            }
                        mapDrone.invalidate();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                Throwable::printStackTrace);
    }

    private void getResourcesMap() {
        resourceService.getRessourceListByInterventionId(
                interventionId,
                response -> {
                    folderResources.getItems().clear();
                    try {
                        List<Resource> resources = Mapper.parseResource(response);
                        for(Resource resource : resources)
                            folderResources.getItems().add(createMarkerResource(resource));
                        mapDrone.invalidate();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                Throwable::printStackTrace);
    }

    private Marker createMarkerPOI(PointOfInterest pointOfInterest) {
        Marker marker = new Marker(mapDrone);
        marker.setPosition(new GeoPoint(pointOfInterest.latitude, pointOfInterest.longitude));
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.setIcon(context.getDrawable(POIUtils.getDrawable(pointOfInterest.type)));
        marker.setInfoWindow(null);
        return marker;
    }

    private Marker createMarkerResource(Resource resource) {
        POIMarkerWithLabel marker = new POIMarkerWithLabel(mapDrone, resource);
        marker.setPosition(new GeoPoint(resource.latitude, resource.longitude));
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM);
        marker.setIcon(context.getDrawable(POIUtils.getDrawable(POIUtils.getResourcePictoTypeFromRole(resource.role, resource.microState, resource.macroState, resource.crm))));
        marker.setInfoWindow(null);
        return marker;
    }
}
