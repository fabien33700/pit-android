package fr.istic.pit.frontend.ui.activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import org.json.JSONException;

import java.util.concurrent.atomic.AtomicBoolean;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.istic.pit.frontend.R;

import fr.istic.pit.frontend.model.Intervention;
import fr.istic.pit.frontend.model.Mapper;
import fr.istic.pit.frontend.service.InterventionService;
import fr.istic.pit.frontend.service.sync.SynchroClientService;
import fr.istic.pit.frontend.service.sync.UpdateEventType;
import fr.istic.pit.frontend.ui.adapters.InterventionViewTabAdapter;
import fr.istic.pit.frontend.ui.fragments.PendingRequestFragment;
import fr.istic.pit.frontend.ui.fragments.DroneFragment;
import fr.istic.pit.frontend.ui.fragments.ResourceTableFragment;
import fr.istic.pit.frontend.ui.fragments.SitacViewFragment;

/**
 * Vue d'une intervention. Contient
 *  - vue SITAC
 *  - tableaux des moyens
 *  - tableau des demandes si connecté en tant que codis
 *  - vue drone s'il y en a un de lié
 */
public class InterventionViewActivity extends AppCompatActivity {

    @BindView(R.id.tabs) TabLayout tabs;
    @BindView(R.id.tabsView) ViewPager tabsView;
    @BindView(R.id.interventionId) TextView interventionIdTextView;

    InterventionViewTabAdapter tabsAdapter;
    SitacViewFragment sitac;

    private Long interventionId;
    private Boolean isCodis;
    private AtomicBoolean hasDrone;

    private SynchroClientService synchroClientService;
    private InterventionService interventionService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intervention_view);
        ButterKnife.bind(this);

        // Recupération de l'intervention id + isCodis
        Intent intent = getIntent();
        interventionId = intent.getLongExtra("interventionId", -1);
        isCodis = intent.getBooleanExtra("isCodis", false);
        Bundle bundle = new Bundle();
        bundle.putLong("interventionId", interventionId);
        bundle.putBoolean("isCodis", isCodis);
        bundle.putString("sinisterCode", intent.getStringExtra("sinisterCode"));
        hasDrone = new AtomicBoolean(false);

        String interText = "INTERVENTION #"+interventionId;
        interventionIdTextView.setText(interText);

        // Ajout des tabs
        tabsAdapter = new InterventionViewTabAdapter(getSupportFragmentManager());

        sitac = new SitacViewFragment();
        sitac.setArguments(bundle);
        tabsAdapter.addFragment(sitac, "SITAC");

        ResourceTableFragment resourceTableFragment = new ResourceTableFragment();
        resourceTableFragment.setArguments(bundle);
        tabsAdapter.addFragment(resourceTableFragment, "Moyens");

        if(isCodis) {
            PendingRequestFragment pendingRequestFragment = new PendingRequestFragment();
            pendingRequestFragment.setArguments(bundle);
            tabsAdapter.addFragment(pendingRequestFragment,"Demandes en attentes");
        }

        tabsView.setAdapter(tabsAdapter);
        tabs.setupWithViewPager(tabsView, true);

        //permission de localisation et de lecture de mémoire pour récup la carte
        ActivityCompat.requestPermissions(InterventionViewActivity.this,
                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        // Recupération sur la présence ou non d'un drone
        synchroClientService = SynchroClientService.getService(this);
        interventionService = InterventionService.getService(this);

        synchroClientService.subscribeTo(interventionId, event -> {
            if(UpdateEventType.REGISTERED_DRONE.equals(event.getType()) && !hasDrone.getAndSet(true)) {
                addTabDrone(true);
            }
        });
        interventionService.getIntervention(
                interventionId,
                response -> {
                    try {
                        Intervention intervention = Mapper.parseIntervention(response);
                        if(!TextUtils.equals("NONE",intervention.droneAvailability) && !hasDrone.getAndSet(true)) {
                            addTabDrone(TextUtils.equals("AVAILABLE", intervention.droneAvailability));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                },
                Throwable::printStackTrace);

        // Dummy 10, pour éviter la recréation des fragments lors des swipes.
        // see : https://stackoverflow.com/questions/24891055/i-dont-want-to-refresh-fragment-on-change
        tabsView.setOffscreenPageLimit(10);
    }

    /**
     * Ajoute un onglet à l'acitivity pour afficher le drone
     * @param droneAvailable true si le drone est disponible
     */
    private void addTabDrone(boolean droneAvailable) {
        Bundle bundle = new Bundle();
        bundle.putLong("interventionId", interventionId);
        bundle.putBoolean("isCodis", isCodis);
        bundle.putBoolean("droneAvailable", droneAvailable);
        DroneFragment droneFragment = new DroneFragment();
        droneFragment.setSitac(sitac);
        droneFragment.setArguments(bundle);
        tabsAdapter.addFragment(droneFragment, "Drone");
        tabsAdapter.notifyDataSetChanged();
    }
}
