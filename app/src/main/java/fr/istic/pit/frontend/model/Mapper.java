package fr.istic.pit.frontend.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import fr.istic.pit.frontend.Utils;

/**
 * Classe utilitaire pour parser ou générer une version JSON des différents modèles
 */
public class Mapper {

    /**
     * Parse un {@link JSONObject} en {@link Intervention}
     *
     * @param jsonObject à parser
     * @return Intervention
     * @throws JSONException si l'objet à parser ne contient pas les champs attendus
     */
    public static Intervention parseIntervention(JSONObject jsonObject) throws JSONException {
        Intervention intervention = new Intervention();
        intervention.id = jsonObject.getLong("id");
        intervention.name = jsonObject.optString("name");
        intervention.sinisterCode = jsonObject.getString("sinisterCode");
        intervention.startTime = Utils.parseDate(jsonObject.getString("startTime"));
        intervention.endTime = Utils.parseDate(jsonObject.optString("endTime"));
        intervention.latitude = jsonObject.getDouble("latitude");
        intervention.longitude = jsonObject.getDouble("longitude");
        intervention.informations = jsonObject.optString("informations");
        intervention.droneAvailability = jsonObject.optString("droneAvailability", "NONE");
        return intervention;
    }

    /**
     * Parse une {@link JSONArray} en liste de {@link Intervention}
     *
     * @param jsonArray représentant des interventions
     * @return liste d'interventions
     * @throws JSONException si au moins un des objets à parser ne contient pas les champs attendus
     */
    public static List<Intervention> parseIntervention(JSONArray jsonArray) throws JSONException {
        List<Intervention> interventions = new LinkedList<>();
        for(int i=0; i<jsonArray.length(); ++i)
            interventions.add(parseIntervention(jsonArray.getJSONObject(i)));
        return interventions;
    }

    /**
     * Crée un {@link JSONObject} à partir d'une {@link Intervention}
     *
     * @param intervention l'intervention à convertir
     * @return un {@link JSONObject} représentant l'intervention en paramètre
     * @throws JSONException si certains champs ne peuvent être convertis
     */
    public static JSONObject interventionToJSON(Intervention intervention) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", intervention.id);
        jsonObject.put("name", intervention.name);
        jsonObject.put("sinisterCode", intervention.sinisterCode);
        jsonObject.put("startTime", intervention.startTime);
        jsonObject.put("endTime", intervention.endTime);
        jsonObject.put("latitude", intervention.latitude);
        jsonObject.put("longitude", intervention.longitude);
        jsonObject.put("informations", intervention.informations);
        jsonObject.put("pointsOfInterest", pointOfInterestToJSON(intervention.pointsOfInterest));
        jsonObject.put("resources", resourceToJSON(intervention.resources));
        jsonObject.put("droneAvailability", intervention.droneAvailability);
        return jsonObject;
    }

    /**
     * Parse un {@link JSONObject} en {@link PointOfInterest}
     *
     * @param jsonObject à parser
     * @return PointOfInterest
     * @throws JSONException si l'objet à parser ne contient pas les champs attendus
     */
    public static PointOfInterest parsePointOfInterest(JSONObject jsonObject) throws JSONException {
        PointOfInterest poi = new PointOfInterest();
        poi.id = jsonObject.getLong("id");
        poi.latitude = jsonObject.getDouble("latitude");
        poi.longitude = jsonObject.getDouble("longitude");
        poi.type = jsonObject.getString("pictoType");
        poi.intervId = jsonObject.getLong("intervId");
        return poi;
    }

    /**
     * Parse une {@link JSONArray} en liste de {@link PointOfInterest}
     *
     * @param jsonArray représentant des pointOfInterest
     * @return liste de PointOfInterest
     * @throws JSONException si au moins un des objets à parser ne contient pas les champs attendus
     */
    public static List<PointOfInterest> parsePointOfInterest(JSONArray jsonArray) throws JSONException {
        List<PointOfInterest> pois = new LinkedList<>();
        for(int i = 0; i < jsonArray.length(); ++i)
            pois.add(parsePointOfInterest(jsonArray.getJSONObject(i)));
        return pois;
    }

    /**
     * Crée un {@link JSONObject} à partir d'un {@link PointOfInterest}
     *
     * @param pointOfInterest à convertir
     * @return un {@link JSONObject} représentant le pointOfInterest en paramètre
     * @throws JSONException si certains champs ne peuvent être convertis
     */
    public static JSONObject pointOfInterestToJSON(PointOfInterest pointOfInterest) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", pointOfInterest.id);
        jsonObject.put("latitude", pointOfInterest.latitude);
        jsonObject.put("longitude", pointOfInterest.longitude);
        jsonObject.put("pictoType", pointOfInterest.type);
        jsonObject.put("intervId", pointOfInterest.intervId);
        return jsonObject;
    }

    /**
     * Crée une {@link JSONArray} à partir d'une liste de {@link PointOfInterest}
     *
     * @param pointOfInterests liste de pointOfInterest à convertir
     * @return une {@link JSONArray} représentant la liste en paramètre
     * @throws JSONException si certains champs ne peuvent être convertis
     */
    public static JSONArray pointOfInterestToJSON(List<PointOfInterest> pointOfInterests) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for(PointOfInterest pointOfInterest : pointOfInterests)
            jsonArray.put(pointOfInterestToJSON(pointOfInterest));
        return jsonArray;
    }

    /**
     * Parse un {@link JSONObject} en {@link Resource}
     *
     * @param jsonObject à parser
     * @return Resource
     * @throws JSONException si l'objet à parser ne contient pas les champs attendus
     */
    public static Resource parseResource(JSONObject jsonObject) throws JSONException {
        Resource resource = new Resource();
        resource.id = jsonObject.getLong("id");
        resource.latitude = jsonObject.optDouble("latitude", 0.0);
        resource.longitude = jsonObject.optDouble("longitude", 0.0);
        resource.role = jsonObject.getInt("role");
        resource.type = jsonObject.getString("pictoType");
        resource.intervId = jsonObject.getLong("intervId");
        resource.name = jsonObject.optString("name", "");
        resource.requestTime = Utils.parseDate(jsonObject.getString("requestTime"));
        resource.startingTime = Utils.parseDate(jsonObject.optString("startingTime"));
        resource.arrivalTime = Utils.parseDate(jsonObject.optString("arrivalTime"));
        resource.releaseTime = Utils.parseDate(jsonObject.optString("releaseTime"));
        resource.macroState = jsonObject.getString("macroState");
        resource.microState = jsonObject.optString("microState");
        resource.crm = jsonObject.getBoolean("crm");
        return resource;
    }

    /**
     * Parse une {@link JSONArray} en liste de {@link Resource}
     *
     * @param jsonArray représentant des resources
     * @return liste de Resource
     * @throws JSONException si au moins un des objets à parser ne contient pas les champs attendus
     */
    public static List<Resource> parseResource(JSONArray jsonArray) throws JSONException {
        List<Resource> resources = new LinkedList<>();
        for(int i=0; i < jsonArray.length(); ++i)
            resources.add(parseResource(jsonArray.getJSONObject(i)));
        return resources;
    }

    /**
     * Crée un {@link JSONObject} à partir d'un {@link Resource}
     *
     * @param resource à convertir
     * @return un {@link JSONObject} représentant la resource en paramètre
     * @throws JSONException si certains champs ne peuvent être convertis
     */
    public static JSONObject resourceToJSON(Resource resource) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", resource.id);
        jsonObject.put("latitude", resource.latitude);
        jsonObject.put("longitude", resource.longitude);
        jsonObject.put("role", resource.role);
        jsonObject.put("pictoType", resource.type);
        jsonObject.put("intervId", resource.intervId);
        jsonObject.put("name", resource.name);
        jsonObject.put("requestTime", resource.requestTime);
        jsonObject.put("startingTime", resource.startingTime);
        jsonObject.put("arrivalTime", resource.arrivalTime);
        jsonObject.put("releaseTime", resource.releaseTime);
        jsonObject.put("macroState", resource.macroState);
        jsonObject.put("microState", resource.microState);
        jsonObject.put("crm", resource.crm);
        return jsonObject;
    }

    /**
     * Crée une {@link JSONArray} à partir d'une liste de {@link Resource}
     *
     * @param resources liste de resources à convertir
     * @return une {@link JSONArray} représentant la liste en paramètre
     * @throws JSONException si certains champs ne peuvent être convertis
     */
    public static JSONArray resourceToJSON(List<Resource> resources) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for(Resource resource : resources)
            jsonArray.put(resourceToJSON(resource));
        return jsonArray;
    }

    /**
     * Parse un {@link JSONObject} en {@link DroneMission}
     *
     * @param jsonObject à parser
     * @return DroneMission
     * @throws JSONException si l'objet à parser ne contient pas les champs attendus
     */
    public static DroneMission parseDroneMission(JSONObject jsonObject) throws JSONException {
        DroneMission droneMission = new DroneMission();
        droneMission.id = jsonObject.getLong("id");
        droneMission.interventionId = jsonObject.getLong("intervId");
        droneMission.currentPoint = jsonObject.optLong("currentPointId");
        droneMission.terminated = jsonObject.optBoolean("terminated", true);
        droneMission.points = new LinkedList<>();
        if(jsonObject.has("points"))
            droneMission.points.addAll(parseDronePoint(jsonObject.getJSONArray("points")));
        droneMission.points.forEach(points -> points.missionId = droneMission.id);
        if(jsonObject.has("lastDroneState")) {
            JSONObject lastDroneState = jsonObject.getJSONObject("lastDroneState");
            droneMission.drone.state = Drone.State.valueOf(lastDroneState.getString("state"));
            droneMission.drone.altitude = lastDroneState.getDouble("altitude");
            droneMission.drone.longitude = lastDroneState.getDouble("longitude");
            droneMission.drone.latitude = lastDroneState.getDouble("latitude");
        }
        droneMission.missionLoop = jsonObject.optBoolean("missionLoop", true);
        return droneMission;
    }

    /**
     * Parse un {@link JSONObject} en {@link DronePoint}
     *
     * @param jsonObject à parser
     * @return DronePoint
     * @throws JSONException si l'objet à parser ne contient pas les champs attendus
     */
    public static DronePoint parseDronePoint(JSONObject jsonObject) throws JSONException {
        DronePoint dronePoint = new DronePoint();
        dronePoint.id = jsonObject.getLong("id");
        dronePoint.missionId = jsonObject.optLong("missionId");
        dronePoint.orderNumber = jsonObject.getInt("orderNumber");
        dronePoint.latitude = jsonObject.getDouble("latitude");
        dronePoint.longitude = jsonObject.getDouble("longitude");
        dronePoint.altitude = jsonObject.getDouble("altitude");
        return dronePoint;
    }

    /**
     * Parse une {@link JSONArray} en liste de {@link DronePoint}
     *
     * @param jsonArray représentant des dronePoints
     * @return liste de dronePoints
     * @throws JSONException si au moins un des objets à parser ne contient pas les champs attendus
     */
    public static List<DronePoint> parseDronePoint(JSONArray jsonArray) throws JSONException {
        List<DronePoint> dronePoints = new LinkedList<>();
        for(int i = 0; i < jsonArray.length(); ++i)
            dronePoints.add(parseDronePoint(jsonArray.getJSONObject(i)));
        return dronePoints;
    }

    /**
     * Crée un {@link JSONObject} à partir d'un {@link DronePoint}
     *
     * @param dronePoint à convertir
     * @return un {@link JSONObject} représentant le dronePoint en paramètre
     * @throws JSONException si certains champs ne peuvent être convertis
     */
    public static JSONObject dronePointToJSONPartial(DronePoint dronePoint) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("latitude", dronePoint.latitude);
        jsonObject.put("longitude", dronePoint.longitude);
        jsonObject.put("altitude", dronePoint.altitude);
        return jsonObject;
    }

    /**
     * Crée une {@link JSONArray} à partir d'une liste de {@link DronePoint}
     *
     * @param dronePoints liste de dronePoints à convertir
     * @return une {@link JSONArray} représentant la liste en paramètre
     * @throws JSONException si certains champs ne peuvent être convertis
     */
    public static JSONArray dronePointToJSONPartial(List<DronePoint> dronePoints) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for(DronePoint dronePoint : dronePoints)
            jsonArray.put(dronePointToJSONPartial(dronePoint));
        return jsonArray;
    }

    /**
     * Parse une {@link JSONArray} en liste de {@link String} qui sont les noms photos
     *
     * @param jsonArray représentant des String
     * @return liste de String
     * @throws JSONException si au moins un des objets à parser ne contient pas les champs attendus
     */
    public static List<String> parsePicturesNames(JSONArray jsonArray) throws JSONException {
        List<String> names = new LinkedList<>();
        for(int i = 0; i < jsonArray.length(); ++i)
            names.add(jsonArray.getString(i));
        return names;
    }
}
