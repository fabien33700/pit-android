package fr.istic.pit.frontend.ui.markers;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import org.osmdroid.views.MapView;

import fr.istic.pit.frontend.model.Resource;

/**
 * Marker avec label, associé à une ressource
 * Si le label est null, le type de la resource s'affiche à la place du label
 */
public class POIMarkerWithLabel extends POIMarker {
        private Paint textPaint;
        private String mLabel;

        public POIMarkerWithLabel(MapView mv, Resource resource) {
            super(mv, resource);
            mLabel = resource.name != null && !resource.name.isEmpty()? resource.name : resource.type;
            textPaint = new Paint();
            textPaint.setColor(Color.BLACK);
            textPaint.setTextSize(17);
            textPaint.setAntiAlias(true);
            textPaint.setTextAlign(Paint.Align.CENTER);
        }

        @Override
        public void draw(final Canvas c, final MapView osmv, boolean shadow) {
            draw(c, osmv);
        }

        public void draw(final Canvas c, final MapView osmv) {
            super.draw(c, osmv, false);
            Point p = this.mPositionPixels;
            c.drawText(mLabel, p.x, p.y-24f, textPaint);
        }
}
