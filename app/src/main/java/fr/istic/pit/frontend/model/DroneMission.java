package fr.istic.pit.frontend.model;

import java.util.LinkedList;
import java.util.List;

/**
 * Classe DTO pour une mission de drone
 */
public class DroneMission {

    /**
     * ID de la mission
     */
    public long id;

    /**
     * ID de l'intervention liée à la mission
     */
    public long interventionId;

    /**
     * Drone de la mission
     */
    public Drone drone = new Drone();

    /**
     * ID du {@link DronePoint} sur lequel le drone se trouve. Peut être null
     */
    public long currentPoint;

    /**
     * Liste des points du parcours du drone
     */
    public List<DronePoint> points = new LinkedList<>();

    /**
     * True si la mission est finie
     */
    public boolean terminated = true;

    /**
     * True si la mission boucle, False si le parcours est ouvert / que le drone fait des A-R
     */
    public boolean missionLoop = true;

    public DroneMission(){}
}
