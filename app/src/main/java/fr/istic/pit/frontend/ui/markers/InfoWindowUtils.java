package fr.istic.pit.frontend.ui.markers;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.infowindow.InfoWindow;

import java.util.List;

import fr.istic.pit.frontend.POIUtils;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.Resource;
import fr.istic.pit.frontend.ui.fragments.SitacViewFragment;

/**
 * Classe utilitaire pour les infowindows
 */
public class InfoWindowUtils {

    private InfoWindowUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Renvoie un click listener qui met à jour le rôle d'une ressource
     * @param sitac le fragment sitac
     * @param marker le marker concerné
     * @param infowWindow l'infowindow qui a été cliquée
     * @return un click listener
     */
    public static View.OnClickListener getColorListener(SitacViewFragment sitac, POIMarker marker, InfoWindow infowWindow){
        return view -> {
            infowWindow.close();
            Context context = infowWindow.getMapView().getContext();
            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            dialogBuilder.setCancelable(true);
            View v = LayoutInflater.from(context).inflate(R.layout.alert_change_color, null, false);
            dialogBuilder.setView(v);
            AlertDialog alert = dialogBuilder.create();
            alert.show();
            Window window = alert.getWindow();
            window.setGravity(Gravity.TOP);
            window.getAttributes().verticalMargin = 0.1F;
            window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            window.setLayout(800, 280);

            final Button water = v.findViewById(R.id.eau);
            final Button fire = v.findViewById(R.id.incendie);
            final Button human = v.findViewById(R.id.humain);
            final Button risk = v.findViewById(R.id.risques);

            water.setOnClickListener(v1 -> {
                updateRole(sitac, POIUtils.EAU, marker);
                alert.cancel();
            });

            fire.setOnClickListener(v12 -> {
                updateRole(sitac, POIUtils.FEU, marker);
                alert.cancel();
            });

            human.setOnClickListener(v13 -> {
                updateRole(sitac, POIUtils.HUMAIN, marker);
                alert.cancel();
            });

            risk.setOnClickListener(v14 -> {
                updateRole(sitac, POIUtils.RISQUE, marker);
                alert.cancel();
            });
        };
    }

    /**
     * Renvoie un click listener qui supprime un poi ou une ressource
     * @param sitac le fragment sitac
     * @param marker le marker concerné
     * @param infowWindow l'infowindow qui a été cliquée
     * @param folder le folderPOI ou le folderResource du fragment sitac
     * @return un click listener
     */
    public static View.OnClickListener getDeleteListener(SitacViewFragment sitac, POIMarker marker, InfoWindow infowWindow, MyRadiusMarkerClusterer folder){
        return v -> {
            infowWindow.close();
            List<Marker> markers = folder.getItems();
            if (markers.remove(marker)){
                if (folder == sitac.folderPOI){
                    sitac.deletePOI(marker.getPoi());
                } else {
                    sitac.moveResourceToCrm((Resource)marker.getPoi());
                }
            }
        };
    }

    /**
     * Renvoie un click listener qui valide la position d'une ressource
     * sur la carte
     * @param sitac le fragment sitac
     * @param marker le marker concerné
     * @param infowWindow l'infowindow qui a été cliquée
     * @return un click listener
     */
    public static View.OnClickListener getValidateListener(SitacViewFragment sitac, POIMarker marker, InfoWindow infowWindow){
        return v -> {
            infowWindow.close();
            Resource resource = (Resource)marker.getPoi();
            sitac.validateResource(resource);
        };
    }

    /**
     * Met à jour le rôle d'une ressource
     * @param sitacViewFragment le fragment sitac
     * @param role le nouveau rôle de la ressource
     * @param marker le marker dont la ressource doit être updatée
     */
    private static void updateRole(SitacViewFragment sitacViewFragment, Integer role, POIMarker marker){
        Resource resource = (Resource) marker.getPoi();
        resource.role = role;
        sitacViewFragment.updateRoleResource(resource);
    }

}
