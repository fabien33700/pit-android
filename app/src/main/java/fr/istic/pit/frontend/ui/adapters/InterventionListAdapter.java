package fr.istic.pit.frontend.ui.adapters;

import android.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.Intervention;

/**
 * Liste des interventions
 */
public class InterventionListAdapter extends RecyclerView.Adapter<InterventionListAdapter.MyViewHolder> {

    private List<Intervention> interventions = new ArrayList<>();
    private InterventionListener interventionListener;

    public InterventionListAdapter(InterventionListener interventionListener) {
        this.interventionListener = interventionListener;
    }

    @Override
    public int getItemCount() {
        return interventions.size();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.activity_intervention_list_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Intervention intervention = interventions.get(position);
        holder.display(intervention);
    }

    public void setInterventions(List<Intervention> interventions) {
        this.interventions = interventions;
        this.notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView name, code, informations;

        private Intervention currentIntervention;

        MyViewHolder(final View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.nameView);
            code = itemView.findViewById(R.id.codeView);
            informations = itemView.findViewById(R.id.infosView);

            Button goButton = itemView.findViewById(R.id.goButton);

            itemView.setOnClickListener(view ->
                    new AlertDialog.Builder(itemView.getContext())
                            .setTitle(currentIntervention.name)
                            .setMessage(currentIntervention.informations)
                            .show());

            goButton.setOnClickListener(view -> interventionListener.onGoToInterventionClicked(interventions.get(getAdapterPosition())));
        }

        void display(Intervention inter) {
            currentIntervention = inter;
            // limiter le nombre de caractères pour éviter un chevauchement
            String theName = currentIntervention.name;
            theName = theName.split("\n")[0];
            if(theName.length()>50){
                theName = theName.substring(0,50);
                theName += "...";
            }
            name.setText(theName);
            code.setText(currentIntervention.sinisterCode);
            // limiter le nombre de caractères pour éviter un chevauchement
            String comment = currentIntervention.informations;
            comment = comment.split("\n")[0];
            if(comment.length()>70){
                comment = comment.substring(0,70);
                comment += "...";
            }
            informations.setText(comment);
        }
    }

    public interface InterventionListener {
        void onGoToInterventionClicked(Intervention intervention);
    }
}
