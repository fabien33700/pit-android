package fr.istic.pit.frontend.ui.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.service.DroneService;

/**
 * Adapter lié à un RecyclerView pour afficher la liste des photos d'un {@link fr.istic.pit.frontend.model.DronePoint}
 * Seuls la latitude, longitude et l'interventioId sont utilisés pour représenter le DronePoint
 */
public class DronePicturesAdapter extends RecyclerView.Adapter<DronePicturesAdapter.PictureViewHolder> {
    /**
     * lie le nom d'une photo à la photo elle-même (son Bitmap)
     */
    private Map<String,Bitmap> picturesMap = new HashMap<>();
    /**
     * Noms des photos
     */
    private List<String> names = new ArrayList<>();

    private Long interventionId;
    private double latitude;
    private double longitude;

    private DroneService droneService;

    public DronePicturesAdapter(Context context, Long interventionId, double latitude, double longitude){
        this.interventionId = interventionId;
        this.latitude = latitude;
        this.longitude = longitude;
        droneService = DroneService.getService(context);
    }

    static class PictureViewHolder extends RecyclerView.ViewHolder {
        ImageView picture;
        TextView name;

        PictureViewHolder(View v) {
            super(v);
            picture = v.findViewById(R.id.pictureItemImage);
            name = v.findViewById(R.id.pictureItemName);
        }
    }

    public void setNames(List<String> names) {
        this.names = names;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PictureViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.picture_item, parent, false);
        return new PictureViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PictureViewHolder holder, int position) {
        String name = names.get(position);

        if(picturesMap.containsKey(name)){
            holder.picture.setImageBitmap(picturesMap.get(name));
        }else{
            droneService.getPhoto(
                    interventionId,
                    latitude,
                    longitude,
                    name,
                    response -> {
                        byte[] decodedString = Base64.decode(response, Base64.DEFAULT);
                        Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                        picturesMap.put(name, decodedByte);
                        holder.picture.setImageBitmap(decodedByte);
                    },
                    Throwable::printStackTrace
            );
        }

        holder.name.setText(name);
    }

    @Override
    public int getItemCount() {
        return names.size();
    }
}
