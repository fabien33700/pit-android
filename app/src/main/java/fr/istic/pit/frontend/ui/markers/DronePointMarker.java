package fr.istic.pit.frontend.ui.markers;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;

import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.DronePoint;

/**
 * Marker affichant un point du parcours du drone
 */
public class DronePointMarker extends Marker {

    private DronePoint dronePoint;
    private Paint textPaint;

    public DronePointMarker(Context context, MapView map, DronePoint dronePoint) {
        super(map);
        this.dronePoint = dronePoint;
        textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(30);
        textPaint.setAntiAlias(true);
        textPaint.setTextAlign(Paint.Align.CENTER);

        this.setPosition(new GeoPoint(dronePoint.latitude, dronePoint.longitude));
        this.setIcon(context.getDrawable(R.drawable.drone_bg));
        this.setAnchor((float)0.5, (float)0.5);
        this.setInfoWindow(null);
    }

    public DronePointMarker(Context context, DronePointMarkerListener listener, DronePoint dronePoint) {
        this(context, listener.getMap(), dronePoint);
        this.setDraggable(true);

        this.setOnMarkerDragListener(new OnMarkerDragListener() {
            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                GeoPoint newPos =  DronePointMarker.this.getPosition();
                dronePoint.latitude = newPos.getLatitude();
                dronePoint.longitude = newPos.getLongitude();
                listener.onMapPointUpdate();
            }

            @Override
            public void onMarkerDragStart(Marker marker) {

            }
        });
    }

    public DronePoint getDronePoint(){
        return dronePoint;
    }

    public void draw(final Canvas c, final MapView osmv, boolean shadow) {
        draw(c, osmv);
    }

    private void draw(final Canvas c, final MapView osmv) {
        super.draw(c, osmv, false);
        Point p = this.mPositionPixels;
        c.drawText(String.valueOf(dronePoint.orderNumber), p.x, p.y, textPaint);
    }

    /**
     * Interface permettant d'intéragir avec cette classe
     */
    public interface DronePointMarkerListener{
        void onMapPointUpdate();
        MapView getMap();
    }

}
