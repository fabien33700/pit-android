package fr.istic.pit.frontend;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Classe utilitaire générale
 */
public class Utils {

    /**
     * Constante : code sinistre d'aide à la personne
     * Est utilisée pour déterminer la couleur des icônes des VLCG
     */
    public static final String ALP = "ALP";

    /**
     * Formate une date pour l'affichage dans le tableau des moyens
     * @param dateString la date à convertir
     * @return une date formatée
     */
    public static Date parseDate(String dateString) {
        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC")); // Timezone de lecture
        try {
            return sdf.parse(dateString);
        } catch (ParseException e) {
            return null;
        }
    }

}
