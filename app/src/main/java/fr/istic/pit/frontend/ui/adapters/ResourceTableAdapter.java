package fr.istic.pit.frontend.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import java.util.TimeZone;

import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.Resource;
import fr.istic.pit.frontend.service.RessourceService;


/**
 * Adapter correspondant au fragment tableau des moyens
 */
public class ResourceTableAdapter extends RecyclerView.Adapter<ResourceTableAdapter.MyViewHolder> {


    private List<Resource> items = new ArrayList<>();

    RessourceService ressourceService;

    boolean isCodis;


    static class MyViewHolder extends RecyclerView.ViewHolder {
        
        // Nom
        TextView name;
         
        // Etat macro
        TextView macroState;
         
        // Heure de demande
        TextView requestTime;
         
        // Heure de départ
        TextView startingTime;
         
        // Heure de départ
        TextView arrivalTime;
        
        // Heure de libération
        TextView releaseTime;

        Button button_cancel; // annulation
        Button button_engaged; // arrival
        Button button_release; // release

        LinearLayout item_layout;


        MyViewHolder(View itemView) {
            super(itemView);

            item_layout = (LinearLayout) itemView.findViewById(R.id.res_tab_item);

            name = itemView.findViewById(R.id.res_table_item_name);
            macroState = itemView.findViewById(R.id.res_table_item_state);
            requestTime = itemView.findViewById(R.id.res_table_item_request_time);
            startingTime = itemView.findViewById(R.id.res_table_item_starting_time);
            arrivalTime = itemView.findViewById(R.id.res_table_item_arrival_time);
            releaseTime = itemView.findViewById(R.id.res_table_item_release_time);

            button_release = itemView.findViewById(R.id.tab_moy_item_button_release);
            button_engaged = itemView.findViewById(R.id.tab_moy_item_bouton_engagement);
            button_cancel = itemView.findViewById(R.id.tab_moy_item_button_cancel);

        }
    }

    public ResourceTableAdapter(Context context, Boolean isCodis) {
        ressourceService = RessourceService.getService(context);
        this.isCodis = isCodis;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.fragment_resource_table_item, parent, false);


        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Resource item = items.get(position);


        Long id_intervention = item.intervId;

        // reset item visibility
        holder.button_release.setVisibility(View.VISIBLE);
        holder.button_engaged.setVisibility(View.VISIBLE);
        holder.button_cancel.setVisibility(View.VISIBLE);

        if (this.isCodis == true) {
            holder.button_cancel.setText("Refuser");
        }
        else if (!this.isCodis) {
            holder.button_cancel.setText(("Annuler"));
        }

        // set item background color to white
        holder.item_layout.setBackgroundColor(Color.rgb(255,255,255));


        // name
        if (item.name == "") {
            holder.name.setText(item.type + " - (pas de nom)");
        }
        else { holder.name.setText(item.type + " - " +item.name); }

        // macroState
        holder.macroState.setText(traductionEtat(item.macroState));


        // request time
        if (item.requestTime == null) {holder.requestTime.setText("---");}
        holder.requestTime.setText(formatDate(item.requestTime));

        // starting time
        if (item.startingTime == null) {
            holder.startingTime.setText("---");
            holder.button_release.setVisibility(View.GONE);
            holder.button_engaged.setVisibility(View.GONE);
        }
        else {
            holder.startingTime.setText(formatDate(item.startingTime));
            holder.button_cancel.setVisibility(View.GONE);
        }

        // arrival time
        if (item.arrivalTime == null) {holder.arrivalTime.setText("---");}
        else {
            holder.arrivalTime.setText(formatDate(item.arrivalTime));
            holder.button_engaged.setVisibility(View.GONE);
        }

        // release time
        if (item.releaseTime == null) {holder.releaseTime.setText("---");}
        else {
            holder.releaseTime.setText(formatDate(item.releaseTime));
            holder.button_release.setVisibility(View.GONE);
            holder.button_engaged.setVisibility(View.GONE);
            holder.button_cancel.setVisibility(View.GONE);

            // set item background color to gainsboro
            holder.item_layout.setBackgroundColor(Color.rgb(220,220,220));

        }



        // buttons
        holder.button_release.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                release(id_intervention,item.id);
            }
        });

        holder.button_engaged.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                engaged(id_intervention,item.id);
            }
        });

        holder.button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancel(id_intervention,item.id,isCodis);
            }
        });



    }

    /*
    ------------------------------------------------------------------------------------------
          methods
    ------------------------------------------------------------------------------------------
     */
    
    @Override
    public int getItemCount() {
        return items.size();
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    public void add(Resource element) {
        items.add(element);
        notifyItemInserted(items.size() - 1);
    }

    public Resource remove(int index) {
        notifyItemRemoved(index);
        return items.remove(index);
    }


    /**
     * Formate la date et l'affiche au bon fuzeau horaire
     * @param date
     * @return La date formatée
     */
    private String formatDate(Date date) {

        SimpleDateFormat dfm = new SimpleDateFormat("dd/MM HH:mm:ss (z)");
        dfm.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));

        return date != null ? dfm.format(date) : "";
    }


    /**
     * Libère le moyen correspondant aux paramètres
     * @param interventionId
     * @param ressourceId
     */
    public void release(Long interventionId, Long ressourceId) {
        ressourceService .releaseRessource(interventionId,ressourceId,
                response -> {
                    Log.d("Response", response.toString());
                }, error -> Log.d("Errors", String.valueOf(error)));
    }


    /**
     * Engage le moyen correspondant aux paramètres
     * @param interventionId
     * @param ressourceId
     */
    public void engaged(Long interventionId, Long ressourceId) {
        ressourceService.arriveRessource(interventionId, ressourceId,
                response -> {
                    Log.d("Response", response.toString());
                }, error -> Log.d("Errors", String.valueOf(error)));
    }


    /**
     * Annule le moyen correspondant aux paramètres
     * @param interventionId
     * @param ressourceId
     * @param isCodis
     */
    public void cancel(Long interventionId, Long ressourceId,Boolean isCodis) {
        ressourceService.rejectRessource(interventionId, ressourceId,isCodis,
                response -> {
                    Log.d("Response", response.toString());
                }, error -> Log.d("Errors", String.valueOf(error)));
    }


    /**
     * Traduit l'état pour affichage dans le tableau des moyens
     * @param etat - état du moyen
     * @return La traduction de l'états
     */
    public String traductionEtat(String etat) {
        String res = etat;

        switch (etat) {
            case "COMING":
                res = "En Route";
                break;
            case "WAITING":
                res = "En Attente";
                break;
            case "ARRIVED":
                res = "Arrivé";
                break;
            case "RELEASED":
                res = "Libéré";
                break;
            case "REJECTED":
                res = "Rejeté";
                break;
            case "END":
                res = "Libéré";
                break;
            case "CANCELLED":
                res = "Annulé";
                break;
        }
        return res;
    }



}
