package fr.istic.pit.frontend.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.Button;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.istic.pit.frontend.R;

import fr.istic.pit.frontend.model.Resource;
import fr.istic.pit.frontend.ui.adapters.ResourceCRMAdapter;

/**
 * Affiche les {@link Resource} liées à cette intervention et qui ne sont pas placées sur la carte
 * Cette liste de resource est passée via un ParcelableArrayListExtra "resources"
 * NB : le choix des resources à afficher ne se fait pas dans cette activity mais bien dans celle qui l'appelle
 */
public class ResourceCRMActivity extends AppCompatActivity {

    @BindView(R.id.addResourceButton) Button addButton;
    @BindView(R.id.cancelAddResourceButton) Button cancelButton;
    @BindView(R.id.resourceCRMRecyclerView) RecyclerView recyclerView;
    private ResourceCRMAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resource_crm);
        ButterKnife.bind(this);

        //LayoutManager
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new ResourceCRMAdapter(getApplicationContext());
        adapter.setOnItemClickListener(position -> {
            addButton.setEnabled(true);
            adapter.setSelectedItem(position);
        });

        ArrayList<Resource> resources = getIntent().getParcelableArrayListExtra("resources");
        adapter.setResources(resources);
        recyclerView.setAdapter(adapter);
        addButton.setEnabled(false);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*.7),(int)(height*.5));
    }

    /**
     * Retourne L'id de la resource sélectionnée "IdResource" via Intent et quitte l'activity
     */
    @OnClick(R.id.addResourceButton)
    public void add(){
        Intent intent = new Intent();
        intent.putExtra("IdResource",adapter.getSelectedItem().id);
        setResult(RESULT_OK,intent);
        finish();
    }

    /**
     * Quitte l'activity
     */
    @OnClick(R.id.cancelAddResourceButton)
    public void cancel(){
        setResult(RESULT_CANCELED);
        finish();
    }
}
