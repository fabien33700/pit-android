package fr.istic.pit.frontend.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.service.LoginService;


/**
 * Ecran de connexion
 */
public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.Identifiant) TextView login;
    @BindView(R.id.Password) TextView password;
    @BindView(R.id.logInCOS) Button logInCOS ;
    @BindView(R.id.logInCODIS) Button logInCODIS ;

    LoginService loginService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        loginService = LoginService.getService(this);
    }

    @OnClick(R.id.logInCODIS)
    public void loginCODIS( View view ) {
        connection(true);
    }

    @OnClick(R.id.logInCOS)
    public void loginCOS( View view ) {
        connection(false);
    }

    private void connection(boolean isCodis) {
        if(TextUtils.isEmpty(login.getText()) || TextUtils.isEmpty(login.getText().toString().trim())
        || TextUtils.isEmpty(password.getText()) || TextUtils.isEmpty(password.getText().toString().trim())) {
            Toast.makeText(LoginActivity.this, "Vérifier les informations renseignées", Toast.LENGTH_SHORT).show();
            return;
        }
        Toast.makeText(LoginActivity.this, "Connexion en cours...", Toast.LENGTH_SHORT).show();
        loginService.login(
                login.getText().toString(),
                password.getText().toString(),
                response -> {
                    Intent nextActivity = new Intent(LoginActivity.this, isCodis ? AccueilActivity.class : InterventionListActivity.class);
                    nextActivity.putExtra("isCodis", isCodis);
                    startActivity(nextActivity);
                    finish();
                },
                error -> Toast.makeText(LoginActivity.this,error.toString(), Toast.LENGTH_LONG).show());
    }
}
