package fr.istic.pit.frontend.ui.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Response;

import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.Intervention;
import fr.istic.pit.frontend.model.Resource;
import fr.istic.pit.frontend.service.AddressToPlaceService;
import fr.istic.pit.frontend.service.InterventionService;
import fr.istic.pit.frontend.ui.adapters.InterventionCreationAdapter;

/**
 * Vue création d'une intervention
 */
public class InterventionCreationActivity extends AppCompatActivity {

    @BindView(R.id.adress) EditText address;
    @BindView(R.id.spinner) Spinner spinner;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.addButton) Button addRessource;
    @BindView(R.id.mapButton) Button mapButton;
    @BindView(R.id.confirm) Button confirm;
    @BindView(R.id.comment) EditText comment;

    InterventionCreationAdapter interventionCreationAdapter;
    InterventionService interventionService;
    AddressToPlaceService addressToPlaceService;

    private boolean isCodis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intervention_creation);
        ButterKnife.bind(this);

        isCodis = getIntent().getBooleanExtra("isCodis", false);

        // Cree un ArrayAdapter basé sur le tableau de string avec le layout de base
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                this,
                R.array.sinister_codes,
                android.R.layout.simple_spinner_item);
        // Specifie le layout a utiliser quand la liste s'ouvre
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Appliquer l'adapter au menu deroulant
        spinner.setAdapter(adapter);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        interventionCreationAdapter = new InterventionCreationAdapter();
        recyclerView.setAdapter(interventionCreationAdapter);

        interventionService = InterventionService.getService(this);
        addressToPlaceService = AddressToPlaceService.getService(this);
    }

    @OnClick(R.id.addButton)
    public void onClickAddRessource(View view) {
        final String[] theResources = getResources().getStringArray(R.array.resources_codes);
        final EditText edittext = new EditText(view.getContext());
        edittext.setHint("nom du moyen");
        edittext.setSingleLine();
        // AlertDialog de creation de nouveau moyen
        AlertDialog alertDialog = new AlertDialog.Builder(view.getContext())
                .setTitle("Type du moyen")
                // selection du type de moyen
                .setSingleChoiceItems(R.array.resources_codes, 0, (inter, pos) -> {})
                // selection du nom du moyen
                .setView(edittext)
                .setPositiveButton(R.string.add, (dialog, id) -> {
                    int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                    String nom = edittext.getText().toString();
                    String type = theResources[selectedPosition];
                    interventionCreationAdapter.newItem(new Pair<>(nom,type));
                })
                .setNegativeButton(R.string.cancel, (dialog, id) -> {}).show();
        alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(v -> {
            if(TextUtils.isEmpty(edittext.getText()) || TextUtils.isEmpty(edittext.getText().toString().trim())) {
                Toast.makeText(getApplicationContext(), "Le nom du moyen doit être renseigné.", Toast.LENGTH_LONG).show();
                return;
            }

            int selectedPosition = alertDialog.getListView().getCheckedItemPosition();
            String nom = edittext.getText().toString();
            String type = theResources[selectedPosition];
            interventionCreationAdapter.newItem(new Pair<>(nom,type));
            alertDialog.dismiss();
        });
    }

    @OnClick(R.id.mapButton)
    public void onClickMapButton(View view) {
        startActivityForResult(new Intent(view.getContext(), InterventionCreationMap.class), 0);
    }

    @OnClick(R.id.confirm)
    public void onClickConfirm(View view) {
        if(TextUtils.isEmpty(address.getText()) || TextUtils.isEmpty(address.getText().toString().trim())) {
            Toast.makeText(view.getContext(), "Sélectionnez un lieu", Toast.LENGTH_LONG).show();
            return;
        }
        if(spinner.getSelectedItem().equals("Code sinistre")) {
            Toast.makeText(view.getContext(), "Sélectionnez un code sinistre", Toast.LENGTH_LONG).show();
            return;
        }
        String[] coordinates = address.getText().toString().split(", ");
        Response.Listener<JSONObject> onSuccess = response -> {
            JSONArray features, coord;
            try {
                features = response.getJSONArray("features");
                if(features.length()==1){
                    coord = features.getJSONObject(0).getJSONObject("geometry").getJSONArray("coordinates");
                    createIntervention(coord.getString(1), coord.getString(0));
                }
                else {
                    String multiFeautures = "Aucun ou plusieurs lieux correspondent à cette adresse.";
                    Toast.makeText(InterventionCreationActivity.this, multiFeautures, Toast.LENGTH_LONG).show();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        };

        Response.ErrorListener onError =
                error -> Toast.makeText(InterventionCreationActivity.this, error.toString(), Toast.LENGTH_LONG).show();

        if(coordinates.length != 2 || !NumberUtils.isNumber(coordinates[0]) || !NumberUtils.isNumber(coordinates[1])){
            if(coordinates.length == 1)
                addressToPlaceService.getCoordinates(
                        coordinates[0],
                        onSuccess,
                        onError
                );
            else if(coordinates.length == 2)
                addressToPlaceService.getCoordinates(
                        coordinates[0],
                        coordinates[1],
                        onSuccess,
                        onError
                );
            else if(coordinates.length == 3)
                addressToPlaceService.getCoordinates(
                        coordinates[0],
                        coordinates[1],
                        coordinates[2],
                        onSuccess,
                        onError
                );
            else Toast.makeText(InterventionCreationActivity.this, "Il y a trop de champs renseignés dans l'adresse.", Toast.LENGTH_LONG).show();
        }
        else {
            createIntervention(coordinates[0], coordinates[1]);
        }
    }


    /**
     * les coordonnées reçues de l'activité "map" sont renseignées dans le champs "lieu"
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 0 && resultCode == RESULT_OK && data.getExtras() != null) {
            String lat = (String) data.getExtras().get("lat");
            String lon = (String) data.getExtras().get("lon");
            String res = lat +", "+ lon;
            this.address.setText(res);
        }
    }

    protected void createIntervention(String latitude, String longitude){
        Intervention intervention = new Intervention();
        intervention.latitude = Double.parseDouble(latitude);
        intervention.longitude = Double.parseDouble(longitude);
        intervention.sinisterCode = spinner.getSelectedItem().toString();
        intervention.name = address.getText().toString();
        intervention.informations = comment.getText().toString();

        List<Resource> resources = new LinkedList<>();
        for(Pair<String, String> resourceData : interventionCreationAdapter.getResources()) {
            Resource resource = new Resource();
            resource.name = resourceData.first;
            resource.type = resourceData.second;
            resources.add(resource);
        }
        intervention.resources.addAll(resources);

        try {
            interventionService.createIntervention(
                    intervention,
                    isCodis,
                    response -> {
                        Toast.makeText(InterventionCreationActivity.this, "L'intervention a bien été créée.", Toast.LENGTH_LONG).show();
                        finish();
                    },
                    error -> Toast.makeText(InterventionCreationActivity.this, error.toString(), Toast.LENGTH_LONG).show());
        } catch (JSONException e) {
            Toast.makeText(InterventionCreationActivity.this, "Erreur lors de la récupération des données de l'intervention à partir du formulaire.", Toast.LENGTH_LONG).show();
        }
    }
}
