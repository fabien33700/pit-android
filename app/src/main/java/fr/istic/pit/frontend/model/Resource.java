package fr.istic.pit.frontend.model;


import android.os.Parcel;
import android.os.Parcelable;

import org.osmdroid.util.GeoPoint;

import java.util.Date;

/**
 * Classe DTO pour les moyens
 */
public class Resource extends PointOfInterest implements Parcelable, Comparable<Resource> {
    /**
     * Le nom/identifiant du moyen
     */
    public String name;

    /**
     * Le rôle du moyen
     */
    public Integer role;

    /**
     * L'heure de la demande
     */
    public Date requestTime;

    /**
     * L'heure de départ
     */
    public Date startingTime;

    /**
     * L'heure d'arrivée
     */
    public Date arrivalTime;

    /**
     * L'heure de libération
     */
    public Date releaseTime;

    /**
     * Etat macro du moyen
     */
    public String macroState;

    /**
     * Etat micro du moyen
     * (en trajet, sur place)
     */
    public String microState;

    /**
     * Indique si le moyen est en attente dans le CRM
     */
    public boolean crm;

    public Resource(GeoPoint point, Integer role, String pictoType, String label) {
        super(point, pictoType);
        this.role = role;
        this.name = label;
    }

    protected Resource(Parcel in) {
        super();
        id = in.readLong();
        name = in.readString();
        type = in.readString();
        role = in.readInt();
        microState = in.readString();
        macroState = in.readString();
        crm = in.readInt() != 0;
    }

    public static final Creator<Resource> CREATOR = new Creator<Resource>() {
        @Override
        public Resource createFromParcel(Parcel in) {
            return new Resource(in);
        }

        @Override
        public Resource[] newArray(int size) {
            return new Resource[size];
        }
    };

    public Resource() {}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(type);
        dest.writeInt(role);
        dest.writeString(microState);
        dest.writeString(macroState);
        dest.writeInt(crm ? 1 : 0);
    }

    @Override
    public int compareTo(Resource o) {
        Date mine = releaseTime != null ? releaseTime : arrivalTime != null ? arrivalTime : startingTime != null ? startingTime : requestTime;
        Date theirs = o.releaseTime != null ? o.releaseTime : o.arrivalTime != null ? o.arrivalTime : o.startingTime != null ? o.startingTime : o.requestTime;
        return theirs.compareTo(mine);
    }
}
