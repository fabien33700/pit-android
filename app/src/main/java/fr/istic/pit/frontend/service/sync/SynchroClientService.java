package fr.istic.pit.frontend.service.sync;

import android.content.Context;
import android.util.Log;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Collections.emptySet;
import static java.util.Objects.requireNonNull;
import static java.util.regex.Pattern.compile;
import static java.util.regex.Pattern.quote;
import static org.eclipse.paho.client.mqttv3.MqttConnectOptions.MQTT_VERSION_3_1_1;

/**
 * Service client pour la synchronisation des données côté front.
 * Il s'agit d'un service doté d'un client MQTT écoutant les évènements
 * de synchronisation, et les redistribuant aux différents callbacks inscrits.
 */
public class SynchroClientService implements MqttCallbackExtended {

    /**
     * Récupération de l'instance du service.
     * @param context Le contexte de l'activité demandant le service
     * @return L'instance du service
     */
    public static SynchroClientService getService(Context context) {
        synchronized (lockGetService) {
            if (serviceInstance == null)
                serviceInstance = new SynchroClientService(context.getApplicationContext());

            return serviceInstance;
        }
    }

    /**
     * Constante : URL du broker MQTT
     */
    private static final String MQTT_BROKER = "tcp://pitprojectm2.ddns.net:12001";

    /**
     * Constante : Topic MQTT de base
     */
    private static final String MQTT_BASE_TOPIC = "pit/sync/events";

    /**
     * Constante : QoS par défaut des messages MQTT
     */
    private static final int MQTT_QOS = 2;

    /**
     * Tag de la classe, pour la journalisation
     */
    private static final String TAG = SynchroClientService.class.getSimpleName();

    /**
     * Un champ stockant les options de connexion
     */
    private static final MqttConnectOptions CONNECT_OPTIONS = connectOptions();

    /**
     * Instance du singleton
     */
    private static SynchroClientService serviceInstance;

    /**
     * Verrou utilisé pour rendre la récupération de l'instance thread-safe
     * (accès concurrents sur le singleton)
     */
    private static final Object lockGetService = new Object();

    /**
     * Le client MQTT
     */
    private final MqttAndroidClient client;

    /**
     * Les callbacks ayant souscrit aux évènements globaux
     */
    private final Set<SynchroCallback> globalSubscribers = new HashSet<>();

    /**
     * Les callbacks ayant souscrit aux évènements par intervention
     */
    private final Map<Long, Set<SynchroCallback>> intervSubscribers = new HashMap<>();

    /**
     * Options pour le tampon de déconnexion.
     * Le tampon de déconnexion permet de stocker les messages à publier lorsque la connexion est
     * perdue pour les réémettre automatiquement à la reconnexion
     * @return Un objet stockant les options du tampon de déconnexion
     */
    private static DisconnectedBufferOptions disconnectedBufferOptions() {
        DisconnectedBufferOptions bufferOpts = new DisconnectedBufferOptions();
        bufferOpts.setBufferEnabled(true);
        bufferOpts.setBufferSize(100);
        bufferOpts.setPersistBuffer(false);
        bufferOpts.setDeleteOldestMessages(false);
        return bufferOpts;
    }

    /**
     * Options de connexion du client au broker MQTT.
     * @return Un objet contenant les options de connexion
     */
    private static MqttConnectOptions connectOptions() {
        MqttConnectOptions options = new MqttConnectOptions();
        options.setAutomaticReconnect(true);
        options.setCleanSession(true);
        options.setMqttVersion(MQTT_VERSION_3_1_1);
        return options;
    }

    /**
     * Constructeur du service (Singleton)
     * @param context Le contexte de l'activité appelante
     */
    private SynchroClientService(Context context) {
        client = new MqttAndroidClient(context, MQTT_BROKER, MqttClient.generateClientId());
        clientConnect();
    }

    /**
     * Enregistre un callback pour les évènements sur le canal global.
     * @param subscriber Le callback
     */
    public void subscribeTo(SynchroCallback subscriber) {
        globalSubscribers.add(
                requireNonNull(subscriber, "subscriber cannot be null"));
    }

    /**
     * Enregistre un callback pour les évènements sur le canal de l'intervention
     * identifiée par l'id passé en paramètre.
     * @param intervId L'id de l'intervention
     * @param subscriber Le callback
     */
    public void subscribeTo(long intervId, SynchroCallback subscriber) {
        requireNonNull(subscriber, "subscriber cannot be null");
        if (!intervSubscribers.containsKey(intervId)) {
            intervSubscribers.put(intervId, new HashSet<>());
            /* On enregistre le premier subscriber pour cette intervention,
             * on prend soin d'abonner le client au topic correspondant */
            clientSubscribe(buildTopic(intervId));
        }

        Set<SynchroCallback> callbacks = intervSubscribers.get(intervId);
        if (callbacks != null)
            callbacks.add(subscriber);
    }

    private void clientSubscribe(String topic) {
        // Tentative d'inscription à un topic
        try {
            if (client.isConnected()) {
                client.subscribe(topic, MQTT_QOS);
                Log.d(TAG, "Subscribed to " + topic);
            }

        } catch (MqttException e) {
            Log.e(TAG, "Could not subscribe to topic " + topic +
                    " on the broker at " + MQTT_BROKER + " because ", e);
        }
    }

    private void clientConnect() {
        // Tentative de connexion du client
        try {
            client.connect(CONNECT_OPTIONS, null, new IMqttActionListener()
            {
                @Override
                public void onSuccess(IMqttToken asyncActionToken)
                {
                    client.setBufferOpts(disconnectedBufferOptions());
                    client.setCallback(SynchroClientService.this);

                    // Évènements globaux
                    clientSubscribe(MQTT_BASE_TOPIC);
                }

                @Override
                public void onFailure(IMqttToken token, Throwable e) {
                    Log.e(TAG, "Could not connect to broker at " + MQTT_BROKER + " because ", e);
                }
            });
        } catch (MqttException e) {
            Log.e(TAG, "Could not connect to broker at " + MQTT_BROKER + " because ", e);
        }
    }

    private Set<SynchroCallback> getSubscribersForTopic(String topic) {
        // Topic de base => subscribers globaux
        if (MQTT_BASE_TOPIC.equals(topic))
            return globalSubscribers;

        // On tente de récupérer un éventuel id d'intervention dans le topic
        Pattern regex = compile(quote(MQTT_BASE_TOPIC + "/") + "(\\d+)$");
        Matcher matcher = regex.matcher(topic);

        if (!matcher.find() || matcher.groupCount() == 0)
            return emptySet();

        // Récupération de l'id
        long intervId = Optional.ofNullable(matcher.group(1))
            .map(Long::parseLong)
            .orElse(0L);

        // Récupération des subscribers pour l'intervention demandée
        return intervSubscribers.getOrDefault(intervId, emptySet());
    }

    /**
     * Construit le topic à partir de l'id de l'intervention,
     * sous la forme {baseTopic}/{id}
     * @param intervId L'id de l'intervention
     * @return Le topic
     */
    private String buildTopic(long intervId) {
        if (intervId > 0) {
            return MQTT_BASE_TOPIC + "/" + intervId;
        }

        return MQTT_BASE_TOPIC;
    }

    /**
     * Consomme un message reçu par le client
     * @param topic Le topic sur lequel l'évènement est arrivé
     * @param json L'objet JSON contenant le message
     */
    private void consumeMessage(String topic, JSONObject json) {
        UpdateEvent event = new UpdateEvent(json);
        Set<SynchroCallback> targetSubscribers = getSubscribersForTopic(topic);

        // Rappel de tous les callbacks inscrits à ce type de message
        if (!targetSubscribers.isEmpty())
            targetSubscribers.forEach(sc -> sc.onUpdateReceived(event) );
    }

    /*** Callbacks du client MQTT ***/
    @Override
    public void connectComplete(boolean reconnect, String brokerUrl) {
        if (reconnect)
            Log.i(TAG,"Reconnected to the broker at " + brokerUrl);
        else
            Log.i(TAG,"Connection successful with the broker at " + brokerUrl);

        try {
            client.subscribe(MQTT_BASE_TOPIC, MQTT_QOS);
            /* Le client se réabonne aux topics de toutes les interventions pour lesquelles
               au moins un callback est enregistré */
            intervSubscribers.keySet()
                    .forEach(id -> clientSubscribe(buildTopic(id)));

        } catch (MqttException e) {
            Log.e(TAG, "Could not subscribe to topic " + MQTT_BASE_TOPIC +
                    " on the broker at " + MQTT_BROKER + " because ", e);
        }
    }

    @Override
    public void connectionLost(Throwable throwable) {
        Log.w(TAG,"Connection with the broker at " + MQTT_BROKER + " lost");
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) {
        // Ignorer les derniers messages des topics et les duplicata
        if (message.isRetained() || message.isDuplicate())
            return;

        Log.d(TAG, "messageArrived : [" + topic + "] :: " + message.toString());
        if (topic.startsWith(MQTT_BASE_TOPIC)) {
            String payload = new String(message.getPayload(), StandardCharsets.UTF_8);
            try {
                JSONObject json = new JSONObject(payload);
                consumeMessage(topic, json);
            } catch (JSONException e) {
                Log.w(TAG, "Could not parse JSON from message " + message.toString(), e);
            }
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken deliveryToken) {
        Log.d(TAG, "deliveryComplete: " + deliveryToken);
    }
}
