package fr.istic.pit.frontend.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import fr.istic.pit.frontend.R;

public class AccueilActivity extends AppCompatActivity {

    @BindView(R.id.interventionList) Button interList;
    @BindView(R.id.interventionCreate) Button interventionCreate;
    @BindView(R.id.waitingRequests) Button waitingRequests;

    // variable pour le click multiple
    private long mLastClickTime = 0;

    boolean isCodis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accueil);
        ButterKnife.bind(this);

        Intent intent = getIntent();
        isCodis = intent.getBooleanExtra("isCodis", false);
    }

    @OnClick(R.id.interventionList)
    public void InterventionList() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        nextActivity(InterventionListActivity.class);
    }

    @OnClick(R.id.interventionCreate)
    public void InterventionCreate() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        nextActivity(InterventionCreationActivity.class);
    }

    @OnClick(R.id.waitingRequests)
    public void WaitingRequests() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
            return;
        }
        mLastClickTime = SystemClock.elapsedRealtime();

        nextActivity(PendingRequestActivity.class);
    }

    private void nextActivity(Class<?> cls)  {
        Intent nextActivity = new Intent(AccueilActivity.this, cls);
        nextActivity.putExtra("isCodis", isCodis);
        startActivity(nextActivity);
    }
}
