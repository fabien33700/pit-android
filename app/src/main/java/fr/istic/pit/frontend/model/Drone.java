package fr.istic.pit.frontend.model;

/**
 * Classe pour le drone
 */
public class Drone {
    /**
     * Etats possibles du drone
     */
    public enum State{MOVING,IDLE,GROUND}

    /**
     * Etat du drone
     */
    public State state = State.GROUND;

    /**
     * Latitude du drone
     */
    public double latitude;

    /**
     * Longitude du drone
     */
    public double longitude;

    /**
     * Altitude du drone
     */
    public double altitude;
}
