package fr.istic.pit.frontend.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.POIUtils;
import fr.istic.pit.frontend.ui.adapters.POIListAdapter;

/**
 * Affiche une liste de POI pour les ajouter sur la carte
 */
public class POIListActivity extends AppCompatActivity {

    @BindView(R.id.addIconButton) Button addButton;
    @BindView(R.id.cancelAddIcon) Button cancelButton;
    @BindView(R.id.listIconRecyclerView) RecyclerView recyclerView;
    private POIListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_poi_list);
        ButterKnife.bind(this);

        //à commenter si la taille des items peut changer suivant le contenu
        recyclerView.setHasFixedSize(true);
        //LayoutManager
        GridLayoutManager layoutManager = new GridLayoutManager(this, 6);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new POIListAdapter(getApplicationContext());
        adapter.setOnItemClickListener(position -> {
            addButton.setEnabled(true);
            adapter.setSelectedItem(position);
        });
        adapter.setIcons(POIUtils.getListOfUniqueIcons());
        recyclerView.setAdapter(adapter);
        addButton.setEnabled(false);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels;
        int height = dm.heightPixels;
        getWindow().setLayout((int)(width*.85),(int)(height*.85));
    }

    /**
     * Retourne L'id de l'icone sélectionnée "IdIcon" via Intent et quitte l'activity
     */
    @OnClick(R.id.addIconButton)
    public void add(){
        Intent intent = new Intent();
        intent.putExtra("IdIcon",adapter.getSelectedItem());
        setResult(RESULT_OK,intent);
        finish();
    }

    /**
     * Quitte l'activity
     */
    @OnClick(R.id.cancelAddIcon)
    public void cancel(){
        setResult(RESULT_CANCELED);
        finish();
    }

}
