package fr.istic.pit.frontend.ui.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import fr.istic.pit.frontend.R;
import fr.istic.pit.frontend.model.DronePoint;

/**
 * Adapter lié à un RecyclerView pour afficher la liste des {@link fr.istic.pit.frontend.model.Drone} d'une {@link fr.istic.pit.frontend.model.DroneMission}
 */
public class DroneParcoursListAdapter extends RecyclerView.Adapter<DroneParcoursListAdapter.LocationViewHolder> {

    private List<DronePoint> points = new ArrayList<>();
    private DroneLocationListListener listener;

    public interface DroneLocationListListener{
        void onChangePosition(DronePoint dronePoint, int newPosition);
        void viewPictures(DronePoint dronePoint);
        void onDeletePoint(DronePoint dronePoint);
        boolean isEditing();
    }

    public DroneParcoursListAdapter(DroneLocationListListener listener){
        this.listener=listener;
    }

    class LocationViewHolder extends RecyclerView.ViewHolder {
        private TextView number;
        private Button upButton;
        private Button downButton;
        private ImageButton deleteButton;

        LocationViewHolder(View v) {
            super(v);

            number = v.findViewById(R.id.droneParcoursFragmentRecyclerViewOrder);
            Button pictureButton = v.findViewById(R.id.droneParcoursFragmentRecyclerViewPicturesButton);
            upButton = v.findViewById(R.id.droneParcoursFragmentRecyclerViewUpButton);
            downButton = v.findViewById(R.id.droneParcoursFragmentRecyclerViewDownButton);
            deleteButton = v.findViewById(R.id.droneParcoursFragmentSupprimerPoint);

            pictureButton.setOnClickListener(view -> listener.viewPictures(points.get(getAdapterPosition())));
            upButton.setOnClickListener(view -> listener.onChangePosition(points.get(getAdapterPosition()),getAdapterPosition()-1));
            downButton.setOnClickListener(view -> listener.onChangePosition(points.get(getAdapterPosition()),getAdapterPosition()+1));
            deleteButton.setOnClickListener(view -> listener.onDeletePoint(points.get(getAdapterPosition())));
        }
    }

    public void setPoints(List<DronePoint> points){
        this.points = points;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public LocationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drone_point_item, parent, false);
        return new LocationViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationViewHolder holder, int position) {
        holder.number.setText(String.valueOf(points.get(position).orderNumber));
        if(listener.isEditing()){
            holder.downButton.setVisibility(View.VISIBLE);
            holder.upButton.setVisibility(View.VISIBLE);
            holder.deleteButton.setVisibility(View.VISIBLE);
            holder.downButton.setEnabled(true);
            holder.upButton.setEnabled(true);
            if(position==0){
                holder.upButton.setEnabled(false);
            }else if(position==points.size()-1){
                holder.downButton.setEnabled(false);
            }
            holder.deleteButton.setVisibility(View.VISIBLE);
        }else{
            holder.downButton.setVisibility(View.GONE);
            holder.upButton.setVisibility(View.GONE);
            holder.deleteButton.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return points.size();
    }

}
